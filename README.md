# OpenHarmony

#### 仓库介绍
本仓库记录OpenHarmony学习过程、OpenHarmony成长计划、OpenHarmony开发板资料

#### 仓库目录
1.  HarmonyOS应用课程的实验报告
2.  HarmonyOS应用课程的期末设计
3.  基于OpenHarmony的智能阳台
4.  基于OpenHarmony的家居安防系统
5.  校园极客秀
